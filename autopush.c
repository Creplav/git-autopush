/*
 * File: autopush.c
 * Last updated: 1/28/2019
 * Author: Ben Doty
 * Overview: 
 * 	This file allows for automatic pushing to git.
 * 	The idea is to use this tool at the end of the day or periodically
 * 	so as to not lose work.
 * 	
 * 	When the command is run it takes in one argument, the git branch.
 * 	If a branch is not supplied then the help for the tool is displayed. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char* argv[]){
	if(argc == 1) {
		printf("Git Autopush\n\n");
		printf("Usage:\n");
		printf("autopush <branch-name>\n");
		printf("\n\n");
	}
	else {
		printf("Auto pushing to branch: %s\n", argv[1]);
		system("git add -A");
		system("git commit -m \"Auto Push\"");
		char pushCommand[50];
		strncat(pushCommand, "git push origin ", 30);
		strncat(pushCommand, argv[1], 30);
		system(pushCommand);
	}
	return 0;
}
