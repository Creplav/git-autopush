# git-autopush

A C utility to auto-push to git

## Install Instructions
1) clone the repo using HTTP or SSH
2) CD into the project directory. 
3) run `make install`
The application should now be installed.

## Usage Instructions
run `autopush` to see the help instructions.

